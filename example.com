__I_ns_foo

$TTL 1H

__I_mx_bar

__I_a_baz

$TTL 5M

gonzo		IN	A	127.45.67.89
gonzo		IN	AAAA	::1
beaker		IN	CNAME	gonzo

$ORIGIN gapps.example.com.

@		IN	MX 10	ASPMX.L.GOOGLE.COM.
@		IN	MX 20	ALT1.ASPMX.L.GOOGLE.COM.
@		IN	MX 20	ALT2.ASPMX.L.GOOGLE.COM.
@		IN	MX 30	ASPMX2.GOOGLEMAIL.COM.
@		IN	MX 30	ASPMX3.GOOGLEMAIL.COM.
@		IN	MX 30	ASPMX4.GOOGLEMAIL.COM.
@		IN	MX 30	ASPMX5.GOOGLEMAIL.COM.

calendar	IN	CNAME	ghs.google.com.
email		IN	CNAME	ghs.google.com.
docs		IN	CNAME	ghs.google.com.
